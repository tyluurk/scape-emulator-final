package net.scapeemulator.game.model;

public final class Position {

	private final int x, y, height;

	public Position(int x, int y) {
		this.x = x;
		this.y = y;
		this.height = 0;
	}

	public Position(int x, int y, int height) {
		this.x = x;
		this.y = y;
		this.height = height;
	}

	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}

	public int getLocalX(int centralRegionX) {
		return x - ((centralRegionX - 6) * 8);
	}

	public int getLocalY(int centralRegionY) {
		return y - ((centralRegionY - 6) * 8);
	}

	public int getCentralRegionX() {
		return x / 8;
	}

	public int getCentralRegionY() {
		return y / 8;
	}

	public int getHeight() {
		return height;
	}

	public boolean isWithinDistance(Position position) {
		int deltaX = position.getX() - x;
		int deltaY = position.getY() - y;
		return deltaX >= -16 && deltaX <= 15 && deltaY >= -16 && deltaY <= 15;
	}

	public int toPackedInt() {
		return (height << 28) | (x << 14) | y;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + height;
		result = prime * result + x;
		result = prime * result + y;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Position other = (Position) obj;
		if (height != other.height)
			return false;
		if (x != other.x)
			return false;
		if (y != other.y)
			return false;
		return true;
	}

}
