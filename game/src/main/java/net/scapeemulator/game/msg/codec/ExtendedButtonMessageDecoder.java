package net.scapeemulator.game.msg.codec;

import net.scapeemulator.game.msg.ExtendedButtonMessage;
import net.scapeemulator.game.net.game.DataType;
import net.scapeemulator.game.net.game.GameFrame;
import net.scapeemulator.game.net.game.GameFrameReader;

import java.io.IOException;

public final class ExtendedButtonMessageDecoder extends MessageDecoder<ExtendedButtonMessage> {

	public ExtendedButtonMessageDecoder() {
		super(155);
	}

	@Override
	public ExtendedButtonMessage decode(GameFrame frame) throws IOException {
		GameFrameReader reader = new GameFrameReader(frame);
		int button = (int) reader.getSigned(DataType.INT);
		int id = (button >> 16) & 0xFFFF;
		int slot = button & 0xFFFF;
		int parameter = (int) reader.getUnsigned(DataType.SHORT);
		return new ExtendedButtonMessage(id, slot, parameter);
	}

}
