package net.scapeemulator.game.model;

public final class GroundObject extends Entity {

	private final int id, type, rotation;

	public GroundObject(int id, int type, Position position, int rotation) {
		this.id = id;
		this.type = type;
		this.position = position;
		this.rotation = rotation;
	}

	public Position getPosition() {
		return position;
	}

	public int getId() {
		return id;
	}

	public int getType() {
		return type;
	}

	public int getRotation() {
		return rotation;
	}

}
