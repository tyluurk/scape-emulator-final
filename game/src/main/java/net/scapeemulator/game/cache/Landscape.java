package net.scapeemulator.game.cache;

import net.scapeemulator.cache.util.ByteBufferUtils;
import net.scapeemulator.game.model.GroundObject;
import net.scapeemulator.game.model.Position;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;

public final class Landscape {

	public static Landscape decode(int x, int y, ByteBuffer buffer) {
		Landscape landscape = new Landscape(x, y);

		int id = -1;
		int deltaId;

		while ((deltaId = ByteBufferUtils.getSmart(buffer)) != 0) {
			id += deltaId;

			int pos = 0;
			int deltaPos;

			while ((deltaPos = ByteBufferUtils.getSmart(buffer)) != 0) {
				pos += deltaPos - 1;

				int localX = (pos >> 6) & 0x3F;
				int localY = pos & 0x3F;
				int height = (pos >> 12) & 0x3;

				int temp = buffer.get() & 0xFF;
				int type = (temp >> 2) & 0x3F;
				int rotation = temp & 0x3;

				Position position = new Position(x * 64 + localX, y * 64 + localY, height);
				landscape.objects.add(new GroundObject(id, type, position, rotation));
			}
		}

		return landscape;
	}

	private final int x, y;
	private final List<GroundObject> objects = new ArrayList<>();

	private Landscape(int x, int y) {
		this.x = x;
		this.y = y;
	}

	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}

	public List<GroundObject> getObjects() {
		return objects;
	}

}
