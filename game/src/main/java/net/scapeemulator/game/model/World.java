package net.scapeemulator.game.model;

import net.scapeemulator.game.net.game.GameSession;
import net.scapeemulator.game.task.TaskScheduler;
import net.scapeemulator.game.update.PlayerUpdater;

public final class World {

	public static final int MAX_PLAYERS = 2000;

	private static final World world = new World();

	public static World getWorld() {
		return world;
	}

	private final MobList<Player> players = new MobList<>(MAX_PLAYERS);
	private final MobList<Npc> npcs = new MobList<>(32000);
	private final TaskScheduler taskScheduler = new TaskScheduler();
	private final PlayerUpdater updater = new PlayerUpdater(this);

	private World() {
		/* empty */
	}

	public MobList<Player> getPlayers() {
		return players;
	}

	public MobList<Npc> getNpcs() {
		return npcs;
	}

	public TaskScheduler getTaskScheduler() {
		return taskScheduler;
	}

	public void tick() {
		for (Player player : players) {
			GameSession session = player.getSession();
			if (session != null)
				session.processMessageQueue();
		}

		taskScheduler.tick();
		updater.tick();
	}

	public Player getPlayerByName(String username) {
		for (Player player : players) {
			if (player.getUsername().equals(username))
				return player;
		}

		return null;
	}

}
