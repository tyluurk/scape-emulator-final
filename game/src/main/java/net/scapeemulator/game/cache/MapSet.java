package net.scapeemulator.game.cache;

import net.scapeemulator.cache.Cache;
import net.scapeemulator.cache.Container;
import net.scapeemulator.cache.ReferenceTable;
import net.scapeemulator.cache.ReferenceTable.Entry;
import net.scapeemulator.cache.util.StringUtils;
import net.scapeemulator.game.util.LandscapeKeyTable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.nio.ByteBuffer;

public final class MapSet {

	private static final Logger logger = LoggerFactory.getLogger(MapSet.class);

	public static void init(Cache cache, LandscapeKeyTable keyTable) throws IOException {
		logger.info("Reading map and landscape files...");

		ReferenceTable rt = ReferenceTable.decode(Container.decode(cache.getStore().read(255, 5)).getData());

		for (int x = 0; x < 256; x++) {
			for (int y = 0; y < 256; y++) {
				String landscapeFile = "l" + x + "_" + y;
				String mapFile = "m" + x + "_" + y;

				int landscapeIdentifier = StringUtils.hash(landscapeFile);
				int mapIdentifier = StringUtils.hash(mapFile);

				for (int id = 0; id < rt.capacity(); id++) {
					Entry entry = rt.getEntry(id);
					if (entry == null)
						continue;

					try {
						if (entry.getIdentifier() == landscapeIdentifier)
							readLandscape(cache, keyTable, x, y, id);
						else if (entry.getIdentifier() == mapIdentifier)
							readMap(cache, x, y, id);
					} catch (Exception ex) {
						logger.debug("Failed to read map/landscape file " + x + ", " + y + ".", ex);
					}
				}
			}
		}
	}

	private static void readLandscape(Cache cache, LandscapeKeyTable keyTable, int x, int y, int id) throws IOException {
		ByteBuffer buffer = cache.getStore().read(5, id);

		int[] key = keyTable.getKeys(x, y);
		buffer = Container.decode(buffer, key).getData();

		Landscape landscape = Landscape.decode(x, y, buffer);
	}

	private static void readMap(Cache cache, int x, int y, int id) throws IOException {
		ByteBuffer buffer = cache.read(5, id).getData();
		Map map = Map.decode(x, y, buffer);
	}

}
