package net.scapeemulator.game.msg.codec;

import net.scapeemulator.game.msg.IdleLogoutMessage;
import net.scapeemulator.game.net.game.GameFrame;

import java.io.IOException;

public final class IdleLogoutMessageDecoder extends MessageDecoder<IdleLogoutMessage> {

	private static final IdleLogoutMessage IDLE_LOGOUT_MESSAGE = new IdleLogoutMessage();

	public IdleLogoutMessageDecoder() {
		super(245);
	}

	@Override
	public IdleLogoutMessage decode(GameFrame frame) throws IOException {
		return IDLE_LOGOUT_MESSAGE;
	}

}
