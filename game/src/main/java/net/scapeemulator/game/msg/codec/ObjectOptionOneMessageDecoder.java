package net.scapeemulator.game.msg.codec;

import net.scapeemulator.game.msg.ObjectOptionOneMessage;
import net.scapeemulator.game.net.game.*;

public final class ObjectOptionOneMessageDecoder extends MessageDecoder<ObjectOptionOneMessage> {

	public ObjectOptionOneMessageDecoder() {
		super(254);
	}

	@Override
	public ObjectOptionOneMessage decode(GameFrame frame) {
		GameFrameReader reader = new GameFrameReader(frame);
		int x = (int) reader.getSigned(DataType.SHORT, DataOrder.LITTLE);
		int id = (int) reader.getSigned(DataType.SHORT, DataTransformation.ADD);
		int y = (int) reader.getSigned(DataType.SHORT);
		return new ObjectOptionOneMessage(x, y, id);
	}

}
