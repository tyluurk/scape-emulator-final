package net.scapeemulator.game.cache;

import net.scapeemulator.game.util.math.PerlinNoise;

import java.nio.ByteBuffer;

public final class Map {

	public static class Tile {

		public static final int FLAG_CLIP = 0x1;
		public static final int FLAG_BRIDGE = 0x2;
		// peterbjornx's refactor suggests a remove roof flag, but is wrong ?
		//public static final int FLAG_ = 0x4; ?
		//public static final int FLAG_ = 0x8; zero logic height ?
		//public static final int FLAG_ = 0x10; seems to make it ignored ?

		private int height;
		private int overlay;
		private int underlay;
		private int shape;
		private int shapeRotation;
		private int flags;

		public int getHeight() {
			return height;
		}

		public int getOverlay() {
			return overlay;
		}

		public int getUnderlay() {
			return underlay;
		}

		public int getShape() {
			return shape;
		}

		public int getShapeRotation() {
			return shapeRotation;
		}

		public int getFlags() {
			return flags;
		}

	}

	public static Map decode(int x, int y, ByteBuffer buffer) {
		Tile[][][] tiles = new Tile[64][64][4];
		for (int plane = 0; plane < 4; plane++) {
			for (int localX = 0; localX < 64; localX++) {
				for (int localY = 0; localY < 64; localY++) {
					tiles[localX][localY][plane] = decodeTile(tiles, localX, localY, plane, buffer);
				}
			}
		}
		return new Map(x, y, tiles);
	}

	private static Tile decodeTile(Tile[][][] tiles, int x, int y, int plane, ByteBuffer buffer) {
		Tile tile = new Tile();
		for (;;) {
			int config = buffer.get() & 0xFF;
			if (config == 0) {
				if (plane == 0)
					tile.height = -PerlinNoise.tileHeight(x + 932731, y + 556238) * 8;
				else
					tile.height = tiles[x][y][plane - 1].height - 240;

				return tile;
			} else if (config == 1) {
				int height = buffer.get() & 0xFF;
				if (height == 1)
					height = 0;

				if (plane == 0)
					tile.height = -height * 8;
				else
					tile.height = tiles[x][y][plane - 1].height - height * 8;

				return tile;
			} else if (config <= 49) {
				tile.overlay = buffer.get() & 0xFF;
				tile.shape = (config - 2) / 4;
				tile.shapeRotation = (config - 2) % 4;
			} else if (config <= 81) {
				tile.flags = config - 49;
			} else {
				tile.underlay = config - 81;
			}
		}
	}

	private final int x, y;
	private final Tile[][][] tiles;

	public Map(int x, int y, Tile[][][] tiles) {
		this.x = x;
		this.y = y;
		this.tiles = tiles;
	}

	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}

	public Tile getTile(int x, int y, int plane) {
		return tiles[x][y][plane];
	}

}
