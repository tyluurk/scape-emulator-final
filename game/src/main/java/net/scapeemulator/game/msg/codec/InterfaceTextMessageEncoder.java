package net.scapeemulator.game.msg.codec;

import io.netty.buffer.ByteBufAllocator;
import net.scapeemulator.game.msg.InterfaceTextMessage;
import net.scapeemulator.game.net.game.*;
import net.scapeemulator.game.net.game.GameFrame.Type;

public final class InterfaceTextMessageEncoder extends MessageEncoder<InterfaceTextMessage> {

	public InterfaceTextMessageEncoder() {
		super(InterfaceTextMessage.class);
	}

	@Override
	public GameFrame encode(ByteBufAllocator alloc, InterfaceTextMessage message) {
		GameFrameBuilder builder = new GameFrameBuilder(alloc, 171, Type.VARIABLE_SHORT);
		builder.put(DataType.INT, DataOrder.INVERSED_MIDDLE, (message.getId() << 16) | message.getSlot());
		builder.putString(message.getText());
		builder.put(DataType.SHORT, DataTransformation.ADD, 0);
		return builder.toGameFrame();
	}

}
